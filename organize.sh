#!/bin/bash

# Arguments
# $ bash organize.sh path/to/src path/to/dst
SRC=$1
DST=$2
export DST # Export DST for make dir(year)

VIDEO_FORMAT=(mp4 avi 3gp mpeg mkv wmv mod)
IMAGE_FORMAT=(jpg jpeg png)


# Funcs
year() {
	ls -l --full-time $1 | cut -d' ' -f6 | cut -d'-' -f1
}
export -f year # Export year fun for another fun

# Make DST dir, If it is'nt exist
if ! [ -a ${DST} ]; then
	mkdir ${DST}
fi
